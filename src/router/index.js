import Vue from 'vue'
import VueRouter from 'vue-router'

// LAYOUTS
import Main from '@/layouts/Main'
import App from '@/layouts/App'

// PAGES
import Exchange from '@/pages/Exchange'
import Funds from '@/pages/Funds'
import Deposit from '@/pages/Deposit'
import Withdraw from '@/pages/Withdraw'
import Account from '@/pages/Account'
import Login from '@/pages/auth/Login'
import Register from '@/pages/auth/Register'
import ForgotPassword from '@/pages/auth/ForgotPassword'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Main,
    children: [
      { path: '/login', component: Login },
      { path: '/register', component: Register },
      { path: '/forgot-password', component: ForgotPassword }
    ]
  },
  {
    path: '/app',
    component: App,
    children: [
      { path: '/', component: Exchange },
      { path: '/exchange', component: Exchange },
      { path: '/funds', component: Funds },
      { path: '/account', component: Account },
      { path: '/deposit', component: Deposit },
      { path: '/withdraw', component: Withdraw }
    ]
  }
]

export default new VueRouter({
  mode: 'history',
  routes
})
